#include<iostream>
#include<fstream>

using namespace std;
int DigitsSum(int);

int main()
{	
	char digit;
	int times=0;
	int sum=0;
	int i = 0;
	while ((cin.get(digit)&&isdigit(digit)))
	{
		sum += digit - 48;
		i++;
	}
	if (i > 1)times++;
	while (sum>9)
	{
		times++;
		sum = DigitsSum(sum);
	}	
	cout << times ;	
}

int DigitsSum(int number)
{
	int sum=0;
	while (number)
	{
		sum += number % 10;
		number /= 10;
	}
	return sum;
}
